variable "env_name" {
  type = string
}

variable "token" {
  sensitive = true
}

variable "gcp_project" {}

variable "kas_address" {}

variable "container-image-demo-be" {
  # demo-be_do_not_change_this_comment
  default = "registry.gitlab.com/matteocao/demo-be:mainec44d7ab"
}

variable "container-image-demo-fe" {
  # demo-fe_do_not_change_this_comment
  default = "matteocao/demo:fe"
}

variable "gcp_region" {
  type        = string
  default     = "us-central1"
  description = "The name of the Google region where the cluster nodes are to be provisioned"
}

variable "cluster_name" {
  type        = string
  default     = "hello-cluster"
  description = "The name of the cluster to appear on the Google Cloud Console"
}

variable "cluster_description" {
  type        = string
  default     = "This cluster is managed by GitLab"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}

variable "machine_type" {
  type        = string
  default     = "n1-standard-4"
  description = "The name of the machine type to use for the cluster nodes"
}

variable "node_count" {
  default     = 1
  description = "The number of cluster nodes"
}

variable "agent_namespace" {
  default     = "gitlab-agent"
  description = "Kubernetes namespace to install the Agent"
}

