
resource "kubernetes_namespace_v1" "environment" {
  metadata {
    name = var.env_name
  }
}

resource "kubernetes_deployment_v1" "demo-be-deployment" {
  metadata {
    name      = "demo-be-deployment"
    namespace = kubernetes_namespace_v1.environment.metadata[0].name
    labels = {
      app = "demo-be"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = { app = "demo-be" }
    }
    template {
      metadata {
        labels = {
          app = "demo-be"
        }
      }
      spec {
        container {
          image   = var.container-image-demo-be
          name    = "demo-be"
          image_pull_policy = "Always"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "demo-be-service" {
  metadata {
    name = "demo-be-service"
    namespace = kubernetes_namespace_v1.environment.metadata[0].name
  }
  spec {
    selector = {
      app = "${kubernetes_deployment_v1.demo-be-deployment.metadata.0.labels.app}"
    }
    port {
      port        = 80
      target_port = 5000
      protocol    = "TCP"
    }
    type = "ClusterIP"
  }
}


resource "kubernetes_deployment_v1" "demo-fe-deployment" {
  metadata {
    name      = "demo-fe-deployment"
    namespace = kubernetes_namespace_v1.environment.metadata[0].name
    labels = {
      app = "demo-fe"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = { app = "demo-fe" }
    }
    template {
      metadata {
        labels = {
          app = "demo-fe"
        }
      }
      spec {
        container {
          image   = var.container-image-demo-fe
          name    = "demo-fe"
          image_pull_policy = "Always"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}


resource "kubernetes_service_v1" "demo-fe-service" {
  metadata {
    name = "demo-fe-service"
    namespace = kubernetes_namespace_v1.environment.metadata[0].name
  }
  spec {
    selector = {
      app = "${kubernetes_deployment_v1.demo-fe-deployment.metadata.0.labels.app}"
    }
    port {
      port        = 80
      target_port = 5002
      protocol    = "TCP"
    }
    type = "NodePort"
  }
}


resource "kubernetes_ingress_v1" "ingress-nginx" {
  wait_for_load_balancer = true
  metadata {
    name = "ingress-nginx"
    namespace = kubernetes_namespace_v1.environment.metadata[0].name
    #annotations = {
    #  "kubernetes.io/ingress.class" = "nginx"
    #}
  }
  spec {
    ingress_class_name = "nginx" # this configures the ingress controller
    rule {
      http {
        path {
          path = "/test/*"
          backend {
            service {
              name = kubernetes_service_v1.demo-fe-service.metadata.0.name
              port {
                number = 80
              }
            }
          }
        }
      }
    }
  }
}

# Display load balancer hostname (typically present in AWS)
output "load_balancer_hostname" {
  value = kubernetes_ingress_v1.ingress-nginx.status.0.load_balancer.0.ingress.0.hostname
}

# Display load balancer IP (typically present in GCP, or using Nginx ingress controller)
output "load_balancer_ip" {
  value = kubernetes_ingress_v1.ingress-nginx.status.0.load_balancer.0.ingress.0.ip
}


