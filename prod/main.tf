locals {
  env_name = "production"
}

variable "token" {}

variable "gcp_project" {}

variable "kas_address" {}

module "global" {
  source = "../global"

  env_name    = local.env_name
  token       = var.token
  gcp_project = var.gcp_project
  kas_address = var.kas_address
}

