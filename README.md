https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html

https://gitlab.com/gitlab-org/configure/examples/gitlab-terraform-gke

get certificate:
```
kubectl get secrets
kubectl get secret <secret name> -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```


```
gcloud container clusters create-auto hello-cluster     --region=us-central1
gcloud container clusters get-credentials hello-cluster --region=us-central1
```
